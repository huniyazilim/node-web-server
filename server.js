const express = require('express');
const hbs = require('hbs');
const fs = require('fs');

// process.env.PORT heroku için buraya eklendi.
// eğer PORT env. değişkeni bulunursa onu kullanacak program
// ki heroku bunu kullanır, bulamaz ise varsayılan 3000 portunu kullanacak.


const port = process.env.PORT || 3000;

var app = express();

hbs.registerPartials(__dirname + "/views/partials");
app.set("view engine", "hbs"); // view engine olarak handlebars kullanacağını bildir.
hbs.registerHelper("getCurrentYear", () => {
  return new Date().getFullYear();
});

hbs.registerHelper("screamIt", (text) => {
  return text.toUpperCase();
})

// app.use(function (req, res, next) {
//   res.render("bakim.hbs");
// });

// "/public" şeklinde yazmaz isen çalışmıyor.
app.use(express.static(__dirname + "/public"));

app.use(function (req, res, next) {
  const now = new Date().toString();
  const logText = `${now} : ${req.method} ${req.url}`;
  console.log(logText);
  fs.appendFile("server.log", logText + "\n", function(err){
    if (err){
      console.log(err);
    }
  });
  next();
});

app.get("/", (req, res) => {
  res.render("home.hbs", {
    pageTitle : "Ana sayfa",
    wellcomeMessage : "Node.js web server sayfasına hoş geldiniz."
  });
});

app.get("/about", (req, res) => {
  res.render("about.hbs", {
    pageTitle : "Hakkında sayfası"
  });
});

app.get("/projects", (req, res) => {
  res.render("projects.hbs", {
    pageTitle : "Projeler sayfası"
  });
});


app.get("/bad", (req, res) => {
  res.json({
    errorMessage : "Bir hata oluştu."
  });
});

app.listen(port, () => {
  console.log("Express server is listenin port " + port);
});
